% load TIFFStacks in current directory
d = dir('*.tif');
nCycles = numel(d);

for indF = 1:nCycles
    im{indF} = TIFFStack(d(indF).name);
end
%%
clear imCorr
registerChan = 1;
nChan = size(im{1},3);

[optimizer, metric] = imregconfig('multimodal');

highPass = 2; % smallest structures to filter
lowPass = 40; % largest structures to filter

nx = size(im{1},1);
imCorr = zeros(nx, nx, nChan, nCycles);
imCorr(:,:,:,1) = im{1}(:,:,:);

for ind = 1:nCycles
    % first pass - affine registration
    tform = imregtform(im{ind}(:,:,registerChan),...
        im{1}(:,:,registerChan), 'similarity', optimizer, metric);
    
    for indCh = 1:nChan
        imCorr(:,:,indCh,ind) = double(imwarp(im{ind}(:,:,indCh), tform, ...
            'OutputView', imref2d([nx,nx])));
        % normalize by mean
        imCorr(:,:,indCh,ind) = imCorr(:,:,indCh,ind) / ...
            mean(reshape(imCorr(:,:,indCh,ind),[],1));
        
        % second pass - elastic registration
        if ind>1    
            [~, imCorr(:,:,indCh,ind) ] = imregdemons(imCorr(:,:,indCh,ind), ...
                imCorr(:,:,indCh,1), [100 40 20]);
        end
    end
end

%%
% check registration quality
figure;
for ind = 1:nCycles
    for indCh = 1:nChan
        ax(ind,indCh) = subplot(nChan,nCycles,ind + (indCh-1)*nCycles);
        imagesc(imCorr(:,:,indCh,ind));
        axis equal square
        caxis(ax(ind,indCh),[2 8])
        colormap gray
    end
end

linkaxes(ax)
%%
% save registered images
dirName = 'processed';
mkdir(dirName);
for indCh = 1:nChan
    fname = ['ch' num2str(indCh) '.tif' ];
    imwrite(uint16(imCorr(:,:,indCh,1)*1e3),...
        fullfile(dirName,fname));
    for ind = 2:nCycles
       imwrite(uint16(imCorr(:,:,indCh,ind)*1e3),...
               fullfile(dirName,fname),...
               'WriteMode','append');
    end
end

%%
% subtract minimum for each channel
imCorr = imCorr - min(imCorr,[],4);
% focus on part of image that Antonin used
imCorr = imCorr(501:1500,501:1500,:,:);
% image for finding rolonies - max intensity across channels
maxImage = max(max(imCorr(:,:,:,:),[],4),[],3);

%%
% subtract Gaussian filtered version of image
h = fspecial('gaussian',15,4);
figure;imagesc(imfilter(maxImage,h));colormap gray; caxis([0 5])
figure;imagesc(maxImage - imfilter(maxImage,h));colormap gray; caxis([0 5]);

filtImage = maxImage - imfilter(maxImage,h);
%%
% threshold to find rolonies
nx = size(filtImage,1);
regions = bwlabel(filtImage>.9);
stats = regionprops(regions);
% filter small regions
idx = find([stats.Area]>2);

regions(~ismember(regions,idx))=0;
regions = reshape(regions,nx,nx);

for ind = 1:numel(idx)
    % make rolony ROIs adding expanding by one pixel
    rois(ind).footprint = imdilate(regions==idx(ind),strel('disk',1));
end

%%
regStack = reshape(imCorr, nx*nx, nChan, nCycles);
% extract fluorescence traces for each ROI
for ind = 1:numel(rois)
    mask = reshape(rois(ind).footprint, nx*nx, 1);
    rois(ind).fluorescence = squeeze(mean(regStack(mask,:,:)));
    
    rois(ind).normf = rois(ind).fluorescence ./ ...
        sqrt(sum(rois(ind).fluorescence .^ 2, 1));    
end

%%
% find the 4 bases using k-means
f = [rois.fluorescence];
f = f(:,sum(f.^2)>.2);
[idx, C] = kmeans(f',4,'distance','cosine');
%%
% check separation
figure;
subplot(2,2,1);
loglog(f(1,idx==1),f(2,idx==1),'.')
hold on
loglog(f(1,idx==2),f(2,idx==2),'.')
loglog(f(1,idx==3),f(2,idx==3),'.')
loglog(f(1,idx==4),f(2,idx==4),'.')

subplot(2,2,2);
loglog(f(3,idx==1),f(4,idx==1),'.')
hold on
loglog(f(3,idx==2),f(4,idx==2),'.')
loglog(f(3,idx==3),f(4,idx==3),'.')
loglog(f(3,idx==4),f(4,idx==4),'.')

subplot(2,2,3);
loglog(f(1,idx==1),f(3,idx==1),'.')
hold on
loglog(f(1,idx==2),f(3,idx==2),'.')
loglog(f(1,idx==3),f(3,idx==3),'.')
loglog(f(1,idx==4),f(3,idx==4),'.')

subplot(2,2,4);
loglog(f(2,idx==1),f(3,idx==1),'.')
hold on
loglog(f(2,idx==2),f(3,idx==2),'.')
loglog(f(2,idx==3),f(3,idx==3),'.')
loglog(f(2,idx==4),f(3,idx==4),'.')
%%
% assign base calls using cosine similarity
for ind = 1:numel(rois)
    [~, rois(ind).basecall] = ...
        min(pdist2(rois(ind).fluorescence', C, 'cosine')');
end
%%
% count how often each sequence occurs
sequences = reshape([rois.basecall],[],numel(rois))';
[barcodes,~,roiIndex] = unique(sequences,'row');
clear nRois
for ind = 1:max(roiIndex)
    nRois(ind) = sum(roiIndex==ind);
end


%%
% plot some example sequence ROIs
nucleotide = 'AGTC';
figure;
imagesc(maxImage); colormap gray; caxis([0 3])
hold on
whichRois = rois(roiIndex==445);

mask = sum(reshape([whichRois.footprint],nx,nx,[]),3);
red = zeros(nx,nx,3);
red(:,:,1) = 1;
h = imshow(red);

set(h,'AlphaData',mask);
title(nucleotide(whichRois(1).basecall))